package com.solvegen;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleControl {

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Answer getUserFromRequest(@RequestBody User user) {
        Answer answer = new Answer();
        answer.setMessage(user.getName() + "!!!");
        return answer;
    }
}





