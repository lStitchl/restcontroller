

package com.solvegen;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Greeting")
public class User {
    @XmlElement(name="name")
    private String name;

    public String getName() {
        return name;
    }
}


